import pygame
import tkinter as tk
from tkinter import *
import os

y: int = 0


def drawing():
    # pygame.draw.circle(screen, (80, 50, 80), (250, 250), 125)
    # pygame.display.update()
    # root.update()
    x: int = 0


def main():
    root = tk.Tk()
    embed = tk.Frame(root, width=800, height=600)  # creates embed frame for pygame window
    embed.grid(columnspan=600, rowspan=500)  # Adds grid
    embed.pack(side=LEFT)  # packs window to the left
    menubar = Menu(root)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Créer un vocabulaire", command=drawing())
    filemenu.add_command(label="Changer vocabulaire", command=drawing())
    filemenu.add_separator()
    filemenu.add_command(label="Quitter", command=root.destroy)
    menubar.add_cascade(label="Options", menu=filemenu)
    root.config(menu=menubar)
    os.environ['SDL_WINDOWID'] = str(embed.winfo_id())
    os.environ['SDL_VIDEODRIVER'] = 'windib'
    screen = pygame.display.set_mode((500, 500))
    screen.fill(pygame.Color(255, 255, 255))
    pygame.display.init()
    while True:
        pygame.display.update()
        root.update()


main()
